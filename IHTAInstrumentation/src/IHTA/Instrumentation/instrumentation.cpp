#include "IHTA/Instrumentation/instrumentation.h"

#include <spdlog/cfg/argv.h>
#include <spdlog/cfg/env.h>
#include <spdlog/details/registry.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

std::vector<spdlog::sink_ptr> IHTA::Instrumentation::LoggerRegistry::sink_registry = { };


void IHTA::Instrumentation::LoggerRegistry::init_logging( )
{
	init_logging( 0, const_cast<const char**>( nullptr ) );
}

void IHTA::Instrumentation::LoggerRegistry::init_logging( int argc, char** argv )
{
	init_logging( argc, const_cast<const char**>( argv ) );
}

void IHTA::Instrumentation::LoggerRegistry::init_logging( int argc, const char** argv )
{
	spdlog::cfg::load_env_levels( );
	spdlog::cfg::load_argv_levels( argc, argv );
}

IHTA::Instrumentation::LoggerRegistry::SinkContainer IHTA::Instrumentation::LoggerRegistry::create_default_sinks( const DefaultSinksConfig& t_config )
{
	SinkContainer return_container;

	bool console_sink_exists = std::any_of( std::begin( sink_registry ), std::end( sink_registry ), []( spdlog::sink_ptr ptr )
	                                        { return static_cast<bool>( std::dynamic_pointer_cast<spdlog::sinks::stdout_color_sink_mt>( ptr ) ); } );
	bool file_sink_exists    = std::any_of( std::begin( sink_registry ), std::end( sink_registry ), []( spdlog::sink_ptr ptr )
	                                        { return static_cast<bool>( std::dynamic_pointer_cast<spdlog::sinks::basic_file_sink_mt>( ptr ) ); } );

	if( !t_config.log_file_name.empty( ) && !file_sink_exists )
	{
		auto fileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>( t_config.log_file_name );
		fileSink->set_level( t_config.file_sink_level );
		fileSink->set_pattern( t_config.file_sink_format );

		return_container.file_sink = fileSink;
		add_sink( fileSink );
	}

	if( !console_sink_exists )
	{
		auto consoleSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>( );
#ifdef _WIN32
		consoleSink->set_color( spdlog::level::info, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE );
#else
		consoleSink->set_color( spdlog::level::level_enum::info, consoleSink->white );
#endif
		consoleSink->set_level( t_config.console_sink_level );
		consoleSink->set_pattern( t_config.console_sink_format );

		return_container.console_sink = consoleSink;
		add_sink( consoleSink );
	}

	return return_container;
}

std::shared_ptr<spdlog::logger> IHTA::Instrumentation::LoggerRegistry::create_logger( std::string t_logger_name, bool t_developer_mode )
{
	auto logger = spdlog::get( t_logger_name );

	if( !logger )
	{
		logger = std::make_shared<spdlog::logger>( t_logger_name );
		spdlog::initialize_logger( logger );

		if( !sink_registry.empty( ) )
		{
			logger->sinks( ).insert( logger->sinks( ).end( ), std::begin( sink_registry ), std::end( sink_registry ) );
		}

		if( t_developer_mode )
		{
			logger->set_level( spdlog::level::level_enum::trace );
		}
	}

	return logger;
}

std::shared_ptr<spdlog::logger> IHTA::Instrumentation::LoggerRegistry::get_logger( std::string t_logger_name, bool t_developer_mode )
{
	auto logger = spdlog::get( t_logger_name );

	if( !logger )
	{
		return create_logger( t_logger_name, t_developer_mode );
	}

	return logger;
}

void IHTA::Instrumentation::LoggerRegistry::add_sink( spdlog::sink_ptr t_sink )
{
	if( std::find( sink_registry.begin( ), sink_registry.end( ), t_sink ) == sink_registry.end( ) )
	{
		sink_registry.push_back( t_sink );
	}
}

void IHTA::Instrumentation::LoggerRegistry::set_sinks_on_loggers( std::vector<spdlog::sink_ptr> t_sinks )
{
	spdlog::details::registry::instance( ).apply_all(
	    [&]( const std::shared_ptr<spdlog::logger> logger )
	    {
		    auto& logger_sinks = logger->sinks( );
		    for( auto&& sink: t_sinks )
		    {
			    if( std::find( logger_sinks.begin( ), logger_sinks.end( ), sink ) == logger_sinks.end( ) )
				    logger->sinks( ).push_back( sink );
		    }
	    } );
}

void IHTA::Instrumentation::LoggerRegistry::drop_all_registered_sinks( )
{
	sink_registry.clear( );
}
