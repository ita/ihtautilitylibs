#include "lib.h"
#include "test_instrumentation.hpp"

#include <iostream>

int main( int argc, const char** argv )
{
	IHTA::Instrumentation::LoggerRegistry::init_logging( argc, argv ); // Initiate the logging

	IHTA::Instrumentation::LoggerRegistry::create_default_sinks( ); // Create default sinks for the logger registry.

	EASY_PROFILER_ENABLE;
	EASY_MAIN_THREAD;

	EASY_FUNCTION( ); // Start an easy profiler block for this function.

	EASY_TEXT( "Name", "text" ); // Add a string to the profile

	TEST_WARN( "Foo" ); // Log with warning level

	TEST_INFO( "Bar" ); // Log with warning level

	TEST_TRACE( "trace" ); // Test trace log (if enabled)

	lib::func( ); // Only if the developer mode is active for the example lib, this function will add profiler blocks.

	lib::header_func( ); // As this function is defined in the header, this function will add profiler blocks depending on the settings for the executable target.

	EASY_END_BLOCK; // Here we end the main function block. This is necessary since `profiler::dumpBlocksToFile( "test_profile.prof" )` does not end any opened blocks.

	auto blocks_count = profiler::dumpBlocksToFile( "test_profile.prof" );

	std::cout << "Blocks count: " << blocks_count << std::endl;

	return 0;
}