#pragma once

#include <lib_instrumentation.hpp>

namespace lib
{
	void localSleep( uint64_t magic = 200000 );

	inline void header_func( )
	{
		EASY_FUNCTION( );

		localSleep( );
	}

	void func( );

} // namespace lib
