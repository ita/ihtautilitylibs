/**
 * \file log_format.h
 * \brief Header containing predefined log formats for spdlog.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2023-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef INCLUDE_WATCHER_IHTA_INSTRUMENTATION_LOG_FORMAT
#define INCLUDE_WATCHER_IHTA_INSTRUMENTATION_LOG_FORMAT

#include <string>

namespace IHTA::Instrumentation::LogFormat
{
	static const std::string only_message         = "%^%v%$";
	static const std::string level_logger_message = "[%^%l%$][%n] %v";
	static const std::string va_like              = "[ %^%-8l%$ ][ %-8n]%v";
	static const std::string spdlog_default       = "%+";
	static const std::string full_detail          = "[%Y-%m-%dT%H:%M:%S.%e] [%^%-8l%$] [%-10n] %-100v [%!] [%@]";
} // namespace IHTA::Instrumentation::LogFormat


#endif