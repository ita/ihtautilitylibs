#include <CLI/App.hpp>
#include <CLI/Config.hpp>
#include <CLI/Formatter.hpp>
#include <IHTA/Tracking/tracking.h>
#include <IHTA/Tracking/utils.h>
#include <csignal>
#include <ctime>
#include <date/date.h>
#include <oscpkt.hh>
#include <sstream>
#include <thread>
#include <udp.hh>

std::atomic_bool quit( false );

inline std::ostream &operator<<( std::ostream &os, const IHTA::Tracking::CTrackingFactory::Types &type )
{
	switch( type )
	{
		case IHTA::Tracking::CTrackingFactory::Types::ART:
			os << "ART";
			break;
		case IHTA::Tracking::CTrackingFactory::Types::Optitrack:
			os << "OptiTrack";
			break;
	}
	return os;
}

std::string getTimezoneOffset( )
{
	const auto now = std::chrono::system_clock::to_time_t( std::chrono::system_clock::now( ) );

	tm lt_loc;
	tm lt_gm;

#ifdef _WIN32
	localtime_s( &lt_loc, &now );
	gmtime_s( &lt_gm, &now );
#else
	localtime_s( &now, &lt_loc );
	gmtime_s( &now, &lt_gm );
#endif

	const auto hourOffset    = lt_loc.tm_hour - lt_gm.tm_hour;
	const auto minutesOffset = lt_loc.tm_min - lt_gm.tm_min;

	std::ostringstream ss;

	ss << ( hourOffset < 0 || minutesOffset < 0 ? '-' : '+' ) << std::setfill( '0' ) << std::setw( 2 ) << std::abs( hourOffset ) << std::setw( 2 )
	   << std::abs( minutesOffset );

	return ss.str( );
}

void oscThreadFunction( std::atomic_bool &programIsRunning, std::atomic_bool &logging, int port )
{
	oscpkt::UdpSocket socket;
	socket.bindTo( port );

	programIsRunning.store( socket.isOk( ) );

	oscpkt::PacketReader reader;

	while( programIsRunning.load( ) && socket.isOk( ) )
	{
		if( socket.receiveNextPacket( 15 ) )
		{
			reader.init( socket.packetData( ), socket.packetSize( ) );

			oscpkt::Message *msg;

			while( reader.isOk( ) && ( msg = reader.popMessage( ) ) != nullptr )
			{
				if( msg->match( "/IHTATrackingLog/start" ).isOkNoMoreArgs( ) )
				{
					logging.store( true );
				}
				else if( msg->match( "/IHTATrackingLog/stop" ).isOkNoMoreArgs( ) )
				{
					logging.store( false );
				}
				else if( msg->match( "/IHTATrackingLog/quit" ).isOkNoMoreArgs( ) )
				{
					quit.store( true );
				}
			}
		}
	}
}

struct LoggingFunction
{
	LoggingFunction( std::atomic_bool &logging, bool verbose_, std::string outFile ) : loggingBool( logging ), verbose( verbose_ ), outputFile( outFile ) {}

	void operator( )( const std::vector<IHTA::Tracking::ITrackingInterface::STrackingDataPoint> &dataPoints )
	{
		if( !isLogging && loggingBool )
		{
			isLogging = true;

			auto logFilePath = std::filesystem::current_path( );
			if( outputFile.empty( ) )
			{
				logFilePath /= "logs";
				logFilePath /=
				    "IHTATrackingLog" + date::format( "%FT%H-%M-%S", floor<std::chrono::seconds>( std::chrono::system_clock::now( ) ) ) + getTimezoneOffset( ) + ".log";
			}
			else
			{
				logFilePath /= outputFile;
			}
			create_directories( logFilePath.parent_path( ) );

			logFile = std::make_shared<std::ofstream>( logFilePath );
		}

		if( isLogging && !loggingBool )
		{
			isLogging = false;
			logFile->close( );
		}

		if( isLogging )
		{
			std::ostringstream ss;
			ss << '[';
			date::to_stream( ss, "%FT%T", std::chrono::system_clock::now( ) );
			ss << getTimezoneOffset( ) << "] [";

			if( !dataPoints.empty( ) )
			{
				ss << dataPoints[0];

				for( auto &&i = std::next( dataPoints.begin( ) ); i != dataPoints.end( ); ++i )
					ss << ',' << *i;
			}

			ss << "]\n";

			*logFile << ss.str( );

			if( verbose )
				std::cout << ss.str( );
		}
	}

	std::atomic_bool &loggingBool;
	bool verbose;
	bool isLogging = false;

	std::shared_ptr<std::ofstream> logFile;

	std::string outputFile;
};

void signalHandler( int signal )
{
	quit.store( true );
}

int main( int argc, char **argv )
{
	CLI::App app {
		R"(
Simple data logger for tracking data.
This app uses the IHTATracking library to receive tracking data
from different tracking systems.

For OptiTrack to work, the server address is required.

For ART to work, the server address is not required. However, if not given,
the measurement must be started via DTrack. If the server address is given,
the measurement is is stated automatically.
Note, this only woks if this application has full access to the tracking
hardware, i.e. no other DTrackSDK is running.

When the trigger-port option is passed, the logging can be triggered via
OSC commands to the respective port.
Per default, this is 3141. However, this default can be overwritten via
the long option like this: --trigger-port=PORT
To start the logging, send the OSC message with the address
	"/IHTATrackingLog/start".
To stop the logging, send the OSC message with the address
	"/IHTATrackingLog/stop".

In order to quit this application, press CTRL+C, close the window or
send "/IHTATrackingLog/quit" via OSC (if triggering is enabled).

Each log entry contains a json encoded array where each element corresponds
to one tracked rigid body.
The elements are json encoded objects with the fields:
v -> valid; int; If '1' the data is a valid tracking point.
c -> coordinate; double array(x,y,z); Cartesian coordinates of the rigid body.
e -> euler angles; double array(roll,pitch,yaw);
                   Euler angle representation of the rigid bodies rotation.
                   The rotation convention is ZYX; roll, pitch, yaw.
q -> quaternion; double array(x,y,z,w); Quaternion representation of the
                                        rigid bodies rotation.

Note, "localhost" is not a valid IP address. To use localhost use "127.0.0.1".
		)"
	};

	const std::map<std::string, IHTA::Tracking::CTrackingFactory::Types> typeMap { { "ART", IHTA::Tracking::CTrackingFactory::Types::ART },
		                                                                           { "OptiTrack", IHTA::Tracking::CTrackingFactory::Types::Optitrack } };

	IHTA::Tracking::CTrackingFactory::Types type { IHTA::Tracking::CTrackingFactory::Types::Optitrack };
	app.add_option( "-b,--backend-type", type, "Tracking backend to be used. Default: OptiTrack" )
	    ->capture_default_str( )
	    ->transform( CLI::CheckedTransformer( typeMap, CLI::ignore_case ) );

	std::string serverAddress;
	const auto serverAddressOption =
	    app.add_option( "-s,--server-address", serverAddress, "IP address of the server, streaming the tracking data, required for OptiTrack. (localhost = 127.0.0.1)" )
	        ->check( CLI::ValidIPV4 );

	int port = -1;
	app.add_option( "-p,--command-port", port, "Optional port of the streaming server for sending commands." )->check( CLI::NonNegativeNumber );

	std::string outputFile;
	app.add_option( "-o,--output-file", outputFile,
	                "Output log file. Default: ./logs/IHTATrackingLog{ISO8601 UTC}.log (Note: If specified, existing file will be overwritten.)" );

	int triggerPort = 3141;
	app.add_flag( "-t{3141},--trigger-port{3141}", triggerPort, "Optional port used for triggering the logging using OSC." );

	bool verbose = false;
	app.add_flag( "-v,--verbose", verbose, "If flag is given, output in console." );

	try
	{
		app.parse( argc, argv );

		if( type == IHTA::Tracking::CTrackingFactory::Types::Optitrack && serverAddress.empty( ) )
			throw CLI::RequiredError( serverAddressOption->get_name( ) );
	}
	catch( const CLI::ParseError &e )
	{
		return app.exit( e );
	}

	std::signal( SIGABRT, signalHandler );
	std::signal( SIGBREAK, signalHandler );
	std::signal( SIGTERM, signalHandler );
	std::signal( SIGINT, signalHandler );

	try
	{
		std::atomic_bool running { true };
		std::atomic_bool logging { ( triggerPort != 0 ? false : true ) };
		std::unique_ptr<std::thread> oscThread;

		if( triggerPort != 0 )
			oscThread = std::make_unique<std::thread>( oscThreadFunction, std::ref( running ), std::ref( logging ), triggerPort );

		auto trackingInterface = IHTA::Tracking::CTrackingFactory::CreateTrackingClass( type );
		if( !trackingInterface->Initialize( "127.0.0.1", serverAddress, port ) )
		{
			std::cerr << "Could not connect to tracker. Exiting!\n";
			return 1;
		}

		trackingInterface->RegisterCallback( LoggingFunction( logging, verbose, outputFile ) );

		while( !quit.load( ) )
		{
			std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
		}

		running = false;
		if( oscThread )
			oscThread->join( );
	}
	catch( const std::exception &e )
	{
		std::cerr << e.what( );
		return 1;
	}

	return 0;
}