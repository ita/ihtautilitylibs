/**
 * \cond DEV
 * \file profiler_data.h
 * \brief Header containing the internal data structures.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2020-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef INCLUDE_WATCHER_IHTA_PROFILER_DATA
#	define INCLUDE_WATCHER_IHTA_PROFILER_DATA

// std includes
#	include <algorithm>
#	include <cassert>
#	include <vector>

// simulation scheduler includes
#	include "IHTA/Profiler/profiler.h"
#	include "IHTA/Profiler/profiler_types.h"

namespace IHTA::Profiler
{
	const int NUMBER_RESERVED_OBJECTS = 1000000;

	///
	/// \brief Profiler data for a section.
	///
	struct ProfileSection
	{
		ProfileSection( ) { vpStartEndTimes.reserve( NUMBER_RESERVED_OBJECTS ); }

		void AddSection( const CSection& oSection ) { vpStartEndTimes.emplace_back( oSection.m_dBegin, oSection.m_dEnd ); }

		///
		/// \brief Vector of all intervals of this section.
		///
		std::vector<std::pair<std::chrono::high_resolution_clock::time_point, std::chrono::high_resolution_clock::time_point>> vpStartEndTimes;
	};

	///
	/// \brief Profiler data for a EventCount.
	///
	/// This class can be used as a simple event or a counter for events.
	///
	struct ProfileEventCount
	{
		ProfileEventCount( ) { vTimeStamps.reserve( NUMBER_RESERVED_OBJECTS ); }

		///
		/// \brief Add another event.
		///
		void operator++( int ) { vTimeStamps.push_back( std::chrono::high_resolution_clock::now( ) ); }

		///
		/// \brief Vector for the time stamps of this EventCount.
		///
		std::vector<std::chrono::time_point<std::chrono::high_resolution_clock>> vTimeStamps;
	};

	//
	/// \brief Profiler data for a Value.
	///
	/// This class can be used to store a vector of values and their insertion times.
	///
	struct ProfileValue
	{
		ProfileValue( ) { voValueData.reserve( NUMBER_RESERVED_OBJECTS ); }

		///
		/// \brief Add a Value to the ProfileValue.
		/// \param valueData the ValueData to be added.
		///
		void AddValue( const ValueData& valueData ) { voValueData.push_back( valueData ); }

		///
		/// \brief Get the data of ProfileValue.
		/// \note This fails is the given type does not match the actual type.
		/// \tparam T the desired type of the data.
		/// \return the data of ProfileValue.
		///
		template<class T>
		std::vector<std::pair<T, double>> GetData( std::chrono::high_resolution_clock::time_point sStartTime ) const
		{
			std::vector<std::pair<T, double>> vData( voValueData.size( ) );

			std::transform( voValueData.begin( ), voValueData.end( ), vData.begin( ),
			                [&]( const ValueData& data ) -> std::pair<T, double>
			                {
				                assert( data.GetType( ) == StdToDataType<T>::eDataType );

				                std::pair<T, double> returnData;
				                returnData.first  = *std::static_pointer_cast<T>( data.GetValue( ) );
				                returnData.second = std::chrono::duration<double>( data.GetTimeStamp( ) - sStartTime ).count( );

				                return returnData;
			                } );

			return vData;
		}

		///
		/// \brief Get the DataType of the ProfileValue.
		/// \return the DataType of the ProfileValue.
		///
		DataType GetDataType( ) const { return voValueData.front( ).GetType( ); }

		///
		/// \brief Get the number of stored values.
		/// \return the number of stored values.
		///
		size_t Size( ) const { return voValueData.size( ); }

	private:
		///
		/// \brief Vector for the ValueData.
		///
		std::vector<ValueData> voValueData;
	};
} // namespace IHTA::Profiler

#endif // INCLUDE_WATCHER_IHTA_PROFILER_DATA

/// \endcond