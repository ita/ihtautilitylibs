# IHTATrackingLogger

This application uses the IHTATracking library to log incoming tracking data.
Currently _OptiTrack_ and _AR-Tracking_ systems are supported.

Use the command line option `--help` or `-h` to get the full list of options and configurations for the application.

## Triggering via OSC

The logging can be triggered via OSC messages.
The accepted OSC addesses are:

| OSC address              | Function             |
|--------------------------|----------------------|
| `/IHTATrackingLog/start` | Start the logging    |
|  `/IHTATrackingLog/stop` | Stop the logging     |
|  `/IHTATrackingLog/quit` | Quit the application |

### Python example

Using the python library [python-osc](https://pypi.org/project/python-osc/):

```python
from pythonosc import udp_client
client = udp_client.SimpleUDPClient("127.0.0.1", 3141)
client.send_message("/IHTATrackingLog/start",[])
# Do some tracking
client.send_message("/IHTATrackingLog/stop",[])

# Close the application
client.send_message("/IHTATrackingLog/quit",[])
```

### Matlab example

Using the [ITA-toolbox](https://git.rwth-aachen.de/ita/toolbox) (requires the Instrument Control Toolbox):

```matlab
osc = itaOSC("127.0.0.1", 3141);
osc.send('/IHTATrackingLog/start');
% Do some tracking
osc.send('/IHTATrackingLog/stop');

% Close the application
osc.send('/IHTATrackingLog/start');
```