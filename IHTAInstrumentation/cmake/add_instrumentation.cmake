include_guard ()

set (
	DEVELOPER_MODE_OVERWRITE
	ON
	CACHE BOOL "If true, allows DEVELOPER_MODE to overwrite log level."
)
mark_as_advanced (DEVELOPER_MODE_OVERWRITE)

#[=======================================================================[.rst:
..command:: target_add_instrumentation

	Function to add instrumentation to a given target.

	- Links to IHTAInstrumentation
	- Generates a custom instrumentation header with target specific instrumentation.
	- Add option to specify target specific compilation log level.
	- Add option for per target development mode.
	  Default value for this is the value for `DEVELOPER_MODE`, which can be set globally.
	  If the development mode is active, the profiler will be activated for the target.
	  Also `${MACRO_PREFIX}_DEVELOPER_MODE` will be set as a compile option to `1` or `0` accordingly.
	  As a result of this, the logger for the target will be set to the trace level, disregarding any previously set log level.

	::

		target_add_instrumentation(
			TARGET_LIBRARY
			[LOGGER_NAME foo]
			[MACRO_PREFIX PRE]
			[INSTRUMENTATION_HEADER instrumentation.h]
			[NAMESPACE IHTA::Instrumentation]
		)

	.. variables:: LOGGER_NAME

		Name of the logger.
		If not specified, target name will be used.

	.. variables:: MACRO_PREFIX

		Prefix for the macros.
		If not specified, capitalized target name will be used.

	.. variables:: CMAKE_PREFIX

		Prefix for the cmake variables.
		If not specified, capitalized target name will be used.

	.. variables:: INSTRUMENTATION_HEADER

		Name for the instrumentation header.
		If not specified, `${TARGET_LIBRARY}_instrumentation.h` will be used.

	.. variables:: NAMESPACE

		Namespace in the instrumentation header.
		Used to define the logger name as `static constexpr std::string_view logger_name` inside it.
		If not specified, `IHTA::${TARGET_LIBRARY}` will be used.

#]=======================================================================]
function (target_add_instrumentation TARGET_LIBRARY)
	set (options)
	set (oneValueArgs LOGGER_NAME MACRO_PREFIX CMAKE_PREFIX INSTRUMENTATION_HEADER NAMESPACE)
	set (multiValueArgs)

	cmake_parse_arguments (TAI "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

	# get_filename_component (TAI_MODULE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
	get_directory_property (LISTFILE_STACK LISTFILE_STACK)
	list (POP_BACK LISTFILE_STACK _LIST_FILE)
	cmake_path (GET _LIST_FILE PARENT_PATH TAI_MODULE_DIR)

	if (NOT TAI_LOGGER_NAME)
		set (TAI_LOGGER_NAME ${TARGET_LIBRARY})
	endif ()

	string (TOUPPER ${TARGET_LIBRARY} TARGET_LIBRARY_UPPER)

	if (NOT TAI_MACRO_PREFIX)
		set (TAI_MACRO_PREFIX ${TARGET_LIBRARY_UPPER})
	endif ()

	if (NOT TAI_CMAKE_PREFIX)
		set (TAI_CMAKE_PREFIX ${TARGET_LIBRARY_UPPER})
	endif ()

	if (NOT TAI_INSTRUMENTATION_HEADER)
		set (TAI_INSTRUMENTATION_HEADER ${TARGET_LIBRARY}_instrumentation.h)
	endif ()

	if (NOT TAI_NAMESPACE)
		set (TAI_NAMESPACE IHTA::${TARGET_LIBRARY})
	endif ()

	set (TAI_HEADER_REL Instrumentation/${TARGET_LIBRARY}/${TAI_INSTRUMENTATION_HEADER})
	cmake_path (NORMAL_PATH TAI_HEADER_REL)
	cmake_path (GET TAI_HEADER_REL PARENT_PATH TAI_HEADER_REL_PATH)

	set (MACRO_PREFIX ${TAI_MACRO_PREFIX})
	set (LOGGER_NAME ${TAI_LOGGER_NAME})
	set (NAMESPACE ${TAI_NAMESPACE})
	configure_file (${TAI_MODULE_DIR}/instrumentation.in ${PROJECT_BINARY_DIR}/${TAI_HEADER_REL} @ONLY)
	unset (MACRO_PREFIX)
	unset (LOGGER_NAME)
	unset (NAMESPACE)

	get_property (
		type
		TARGET ${TARGET_LIBRARY}
		PROPERTY TYPE
	)

	set (
		${TAI_CMAKE_PREFIX}_LOG_LEVEL
		"2"
		CACHE STRING "Logger level between 0 (Trace) and 6 (Off)"
	)
	set_property (
		CACHE ${TAI_CMAKE_PREFIX}_LOG_LEVEL
		PROPERTY STRINGS
				 "0"
				 "1"
				 "2"
				 "3"
				 "4"
				 "5"
				 "6"
	)

	option (${TAI_CMAKE_PREFIX}_DEVELOPER_MODE "Enable developer mode for ${TARGET_LIBRARY}" ${DEVELOPER_MODE})

	set (TAI_INSTALL_INCLUDE_DIR Instrumentation/${TARGET_LIBRARY})

	if (${type} STREQUAL "INTERFACE_LIBRARY")
		target_include_directories (
			${TARGET_LIBRARY} INTERFACE $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/${TAI_HEADER_REL_PATH}>
										$<INSTALL_INTERFACE:include/${TAI_HEADER_REL_PATH}>
		)
		target_link_libraries (${TARGET_LIBRARY} INTERFACE IHTA::IHTAInstrumentation easy_profiler)
		target_compile_definitions (
			${TARGET_LIBRARY}
			INTERFACE
				"${TAI_MACRO_PREFIX}_LOG_LEVEL=${${TAI_CMAKE_PREFIX}_LOG_LEVEL}"
				"$<IF:$<AND:$<BOOL:${${TAI_CMAKE_PREFIX}_DEVELOPER_MODE}>,$<BOOL:${DEVELOPER_MODE_OVERWRITE}>>,${TAI_MACRO_PREFIX}_DEVELOPER_MODE=1,${TAI_MACRO_PREFIX}_DEVELOPER_MODE=0>"
		)
	else ()
		target_include_directories (
			${TARGET_LIBRARY} PUBLIC $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/${TAI_HEADER_REL_PATH}>
									 $<INSTALL_INTERFACE:include/${TAI_HEADER_REL_PATH}>
		)
		target_link_libraries (${TARGET_LIBRARY} PUBLIC IHTA::IHTAInstrumentation easy_profiler)
		target_compile_definitions (
			${TARGET_LIBRARY}
			PUBLIC
				"${TAI_MACRO_PREFIX}_LOG_LEVEL=${${TAI_CMAKE_PREFIX}_LOG_LEVEL}"
				"$<IF:$<AND:$<BOOL:${${TAI_CMAKE_PREFIX}_DEVELOPER_MODE}>,$<BOOL:${DEVELOPER_MODE_OVERWRITE}>>,${TAI_MACRO_PREFIX}_DEVELOPER_MODE=1,${TAI_MACRO_PREFIX}_DEVELOPER_MODE=0>"
			PRIVATE "$<$<NOT:$<BOOL:${${TAI_CMAKE_PREFIX}_DEVELOPER_MODE}>>:DISABLE_EASY_PROFILER>"
		)
	endif ()

	install (
		DIRECTORY ${PROJECT_BINARY_DIR}/${TAI_HEADER_REL_PATH}/
		DESTINATION include/${TAI_HEADER_REL_PATH}
		COMPONENT "${TARGET_LIBRARY}_Development"
	)
endfunction ()
