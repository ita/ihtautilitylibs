/**
 * \file matlab_sink.h
 * \brief Header implementing a native Matlab log sink.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2023-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef INCLUDE_WATCHER_IHTA_INSTRUMENTATION_MATLAB_SINK
#define INCLUDE_WATCHER_IHTA_INSTRUMENTATION_MATLAB_SINK

#ifdef MATLAB_MEX_FILE

#	include "spdlog/details/null_mutex.h"
#	include "spdlog/sinks/base_sink.h"

#	include <mutex>

#	include "mex.h"

namespace IHTA::Instrumentation
{
	///
	/// \brief Sink for logging to Matlab
	///
	/// Implements a spdlog sink that prints directly to the Matlab console.
	/// Based on this https://github.com/gabime/spdlog/wiki/4.-Sinks#implementing-your-own-sink
	///
	template<typename Mutex>
	class MatlabSink : public spdlog::sinks::base_sink<Mutex>
	{
	protected:
		void sink_it_( const spdlog::details::log_msg& msg ) override
		{
			spdlog::memory_buf_t formatted;
			spdlog::sinks::base_sink<Mutex>::formatter_->format( msg, formatted );

			std::string string_formatted( formatted.data( ), formatted.size( ) );
			// Remove carriage return, this causes an extra blank line in matlab console.
			string_formatted.erase( std::remove( string_formatted.begin( ), string_formatted.end( ), '\r' ), string_formatted.cend( ) );

			mexPrintf( string_formatted.c_str( ) );
		}

		void flush_( ) override {}
	};
	using matlab_sink_mt = MatlabSink<std::mutex>;
	using matlab_sink_st = MatlabSink<spdlog::details::null_mutex>;

} // namespace IHTA::Instrumentation

#endif

#endif