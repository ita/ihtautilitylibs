# IHTAUtilityLibs

The IHTA utility libraries are a package of small to medium libraries that may be useful in multiple other projects.
For that reason, they are collected here, so it is easier to include them elsewhere.

Every library is supposed to work standalone and should require minimal dependencies itself.

For further information on the libraries please follow these links to their respective README's:

- [IHTA Profiler](IHTAProfiler/README.md)
- [IHTA Tracking](IHTATracking/README.md)
- [IHTA Instrumentation](IHTAInstrumentation/README.md)

## How to build

It is recommended to build the libraries via the top level CMakeLists file.
The libraries that are not required can be switched off via the CMake options `WITH_IHTA_TRACKING`, `WITH_IHTA_PROFILER` and `WITH_IHTA_INSTRUMENTATION`.
However, it is also possible to build the libraries standalone.
