/**
 * \cond DEV
 * \file ART_tracking.h
 * \brief Header for ART backend.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2021-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef INCLUDE_WATCHER_IHTA_ART_TRACKING
#	define INCLUDE_WATCHER_IHTA_ART_TRACKING

#	include <DTrackSDK.hpp>
#	include <IHTA/Tracking/tracking.h>
#	include <algorithm>
#	include <atomic>
#	include <sstream>
#	include <thread>

namespace IHTA::Tracking
{
	///
	/// \brief Concrete implementation for the AR-Tracking backend.
	///
	class CARTTracking : public ITrackingInterface
	{
	public:
		///
		/// \brief Destructor for CARTTracking.
		///
		virtual ~CARTTracking( )
		{
			if( bIsInitialized )
			{
				bCallbackThreadStopFlag = true;
				oCallbackThread.join( );

				if( bCommandMode )
					pDTrack->stopMeasurement( );
			}
		}

		///
		/// \copydoc ITrackingInterface::Initialize
		///
		bool Initialize( const std::string& sLocalAddress, const std::string& sServerAddress, int port ) override
		{
			if( port < 0 ) // use the default port
			{
				port = 5000;
			}

			std::ostringstream oss;
			bCommandMode = false;

			if( !sServerAddress.empty( ) || sServerAddress == "127.0.0.1" )
			{
				bCommandMode = true;
				oss << sServerAddress << ':';
			}

			oss << port;

			pDTrack = std::make_unique<DTrackSDK>( oss.str( ) );

			if( !pDTrack->isDataInterfaceValid( ) )
			{
				return false;
			}

			if( bCommandMode && pDTrack->isCommandInterfaceValid( ) )
			{
				std::string par;
				const bool isOk = pDTrack->getParam( "system", "access", par ); // ensure full access for DTrack2 commands

				if( !isOk || par != "full" )
				{
					pDTrack = nullptr;
					return false;
				}
			}
			else if( bCommandMode )
			{
				pDTrack = nullptr;
				return false;
			}

			if( bCommandMode && pDTrack->isCommandInterfaceValid( ) )
			{
				if( !pDTrack->startMeasurement( ) )
				{
					pDTrack = nullptr;
					return false;
				}
			}

			bCallbackThreadStopFlag = false;

			oCallbackThread = std::thread(
			    [this]( )
			    {
				    while( !bCallbackThreadStopFlag )
				    {
					    if( pDTrack->receive( ) )
					    {
						    std::vector<STrackingDataPoint> dataPoints;
						    dataPoints.reserve( pDTrack->getNumBody( ) );

						    // Standard bodies:
						    for( int i = 0; i < pDTrack->getNumBody( ); i++ )
						    {
							    const DTrackBody* body = pDTrack->getBody( i );
							    dataPoints.emplace_back( );

							    if( body == NULL )
							    {
								    break;
							    }

							    if( body->isTracked( ) )
							    {
								    dataPoints.back( ).bValid = true;

								    dataPoints.back( ).oLocation = std::array<double, 3> { body->loc[0], body->loc[1], body->loc[2] };

								    std::for_each( dataPoints.back( ).oLocation.begin( ), dataPoints.back( ).oLocation.end( ),
								                   []( double& val )
								                   {
									                   val /= 1e3; // Convert from mm to m
								                   } );

								    const DTrackQuaternion quat    = body->getQuaternion( );
								    dataPoints.back( ).oQuaternion = std::array<double, 4> { quat.x, quat.y, quat.z, quat.w };
							    }
						    }

						    if( oCallback )
						    {
							    oCallback( dataPoints );
						    }
					    }
				    }
			    } );

			if( bCommandMode )
				pDTrack->startMeasurement( );

			bIsInitialized = true;

			return true;
		}

		///
		/// \copydoc ITrackingInterface::RegisterCallback
		///
		bool RegisterCallback( std::function<void( const std::vector<STrackingDataPoint>& oDataPoints )> callback ) override
		{
			oCallback = callback;
			return false;
		}

		std::unique_ptr<DTrackSDK> pDTrack; ///< Pointer to the DTrack client.

		std::thread oCallbackThread; ///< Callback tread, calling the callback function when receiving a new data packet.

		std::atomic<bool> bCallbackThreadStopFlag; ///< Atomic bool for synchronizing/stopping the callback tread.

		std::function<void( const std::vector<STrackingDataPoint>& )> oCallback; ///< Callback function for this tracking interface.

		bool bIsInitialized = false; ///< If true, the interface is initialized.

		bool bCommandMode = false; ///< If true, the interface is in command mode (able to start and stop the tracking).
	};
} // namespace IHTA::Tracking
#endif

/// \endcond