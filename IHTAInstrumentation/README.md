# IHTA Instrumentation

IHTA instrumentation is a helper library to add unified instrumentation to a library.
This includes profiling as well as logging.
For this, the [easy_profiler](https://github.com/yse/easy_profiler) and [spdlog](https://github.com/gabime/spdlog) are used.

## Usage

In order to add instrumentation to a target add

```cmake
target_add_instrumentation ( target_name )
```

to your `CMakeLists.txt` file.
The call of `target_add_instrumentation` can be customized.
See [the documentation](cmake/add_instrumentation.cmake) for further information.

In your `cpp` files you can then add the target specific instrumentation header and call the logger and the profiler.
For a thorough explanation see the docuementation of [easy_profiler](https://github.com/yse/easy_profiler) and [spdlog](https://github.com/gabime/spdlog).
Note however, that `target_add_instrumentation` add target specific logging macros that should be used.

For a more complete example, enable `IHTAINSTRUMENTATION_EXAMPLES` when configuring this library and build the `instrumentation_example_exe`.

## Configuration

The instrumentation of this library can be configured to adapt to various setups.
First of all, a target specific the CMake variable for the log level is added.
For a target `Foo` this will be called `FOO_LOG_LEVEL` (except if `MACRO_PREFIX` is set in the `target_add_instrumentation` call).
It can have a value between zero and six.
Where zero will enable logging from trace level and six will disable logging.
As this is implemented via the preprocessor, the relevant calls will be removed before compilation such that no performance should be lost to the calls.

In addition, a target specific option called `FOO_DEVELOPER_MODE` (again, except if `target_add_instrumentation` is called with `MACRO_PREFIX`) is added.
It is instantiated by the global variable `DEVELOPER_MODE`.
If this is not set, it is assumed to be `OFF`.
When this is set to a truthy value, the profiler will be enabled and the log level will be overwritten such that trace level logging is enabled.
This behaviour can be overwritten via the advanced variable `DEVELOPER_MODE_OVERWRITE`.
Note, that this is not target specific.

Lastly, the logging can be configured at runtime via environment variable and commadline arguments.
See [this](https://github.com/gabime/spdlog#load-log-levels-from-the-env-variable-or-argv) for more info.
