add_library (instrumentation_lib_example STATIC example_lib/lib.h example_lib/lib.cpp)

target_add_instrumentation (
	instrumentation_lib_example
	LOGGER_NAME
	LibLog
	MACRO_PREFIX
	LIB
	INSTRUMENTATION_HEADER
	lib_instrumentation.hpp
)

target_include_directories (instrumentation_lib_example PUBLIC example_lib)

add_executable (instrumentation_example_exe example_exe/example_exe.cpp)
target_add_instrumentation (
	instrumentation_example_exe
	LOGGER_NAME
	Logger
	MACRO_PREFIX
	TEST
	INSTRUMENTATION_HEADER
	test_instrumentation.hpp
)

target_link_libraries (instrumentation_example_exe PUBLIC instrumentation_lib_example)

set_target_properties (
	instrumentation_lib_example instrumentation_example_exe PROPERTIES FOLDER "examples/IHTAInstrumentation"
)
