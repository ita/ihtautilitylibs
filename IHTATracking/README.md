# IHTA Tracking

IHTA tracking is a wrapper library to support different kinds of tracking systems.
In its current implementation, it supports [OptiTack](https://optitrack.com/) and [AR-Tracking](https://ar-tracking.com/).

## IHTA Tracking Logger

The application IHTA Tracking Logger can be used to log tracking data from the different tracking systems to file.
The logging can be triggered via OSC commands.