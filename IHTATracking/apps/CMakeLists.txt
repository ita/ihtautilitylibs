cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

CPMAddPackage (
	NAME CLI11
	GIT_REPOSITORY https://github.com/CLIUtils/CLI11
	GIT_TAG v2.1.2
)

CPMAddPackage (
	NAME oscpkt
	URL http://gruntthepeon.free.fr/oscpkt/oscpkt-1.2.tar.gz
	DOWNLOAD_ONLY TRUE
	DOWNLOAD_EXTRACT_TIMESTAMP TRUE
)

if (oscpkt_ADDED)
	add_library (oscpkt INTERFACE ${oscpkt_SOURCE_DIR}/oscpkt.hh ${oscpkt_SOURCE_DIR}/udp.hh)
	target_include_directories (oscpkt INTERFACE ${oscpkt_SOURCE_DIR})
	target_compile_definitions (oscpkt INTERFACE OSCPKT_OSTREAM_OUTPUT)
	set_property (TARGET oscpkt PROPERTY FOLDER "external_libs")
endif ()

CPMAddPackage (
	NAME date
	GIT_REPOSITORY https://github.com/HowardHinnant/date
	GIT_TAG v3.0.1
)

add_executable (IHTATrackingLogger IHTATrackingLogger.cpp)
target_link_libraries (IHTATrackingLogger PUBLIC IHTA::IHTATracking CLI11::CLI11 date::date oscpkt)

target_compile_features (IHTATrackingLogger PUBLIC cxx_std_17)

set_property (TARGET IHTATrackingLogger PROPERTY FOLDER "Apps/IHTATracking")

set (IHTA_TRACKING_INSTALL_DIR "IHTATrackingLogger_v${PROJECT_VERSION}")

install (TARGETS IHTATrackingLogger RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
install (TARGETS IHTATrackingLogger RUNTIME DESTINATION ${IHTA_TRACKING_INSTALL_DIR}/bin COMPONENT IHTATrackingLogger)

if (WIN32)
	install (
		FILES ${NatNetSDK_BINARIES} $<TARGET_FILE:IHTA::IHTATracking>
		DESTINATION ${IHTA_TRACKING_INSTALL_DIR}/bin
		COMPONENT IHTATrackingLogger
	)
	install (FILES)
endif ()

install (
	FILES README.md
	DESTINATION ${IHTA_TRACKING_INSTALL_DIR}
	COMPONENT IHTATrackingLogger
)
