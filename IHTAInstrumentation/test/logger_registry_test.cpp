#include <IHTA/Instrumentation/instrumentation.h>
#include <catch2/catch_all.hpp>
#include <regex>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/null_sink.h>
#include <spdlog/sinks/ostream_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#define GENERATE_RANDOM_STRING( length ) \
	GENERATE( map( []( const std::vector<int>& i ) { return std::string( i.begin( ), i.end( ) ); }, chunk( length, take( length, random( 32, 122 ) ) ) ) )

std::string escape_for_regex( const std::string& s )
{
	std::regex re( R"(\\|\^|\$|\*|\+|\?|\.|\(|\)|\||\{|\}|\[|\])" );
	return std::regex_replace( s, re, "\\$&" );
};

TEST_CASE( "IHTAInstrumentation::LoggerRegistry/LoggerCreation", "[IHTAInstrumentation][LoggerCreation][PrioHigh]" )
{
	IHTA::Instrumentation::LoggerRegistry::drop_all_registered_sinks( );
	auto dev_mode = GENERATE( true, false );

	SECTION( "New logger with no sinks" )
	{
		std::string logger_name = dev_mode ? "NoSinksDevMode" : "NoSinks";

		auto logger = IHTA::Instrumentation::LoggerRegistry::create_logger( logger_name, dev_mode );

		REQUIRE( logger->sinks( ).empty( ) );
		REQUIRE( logger->level( ) == ( dev_mode ? spdlog::level::level_enum::trace : spdlog::level::level_enum::info ) );

		SECTION( "No double creation" )
		{
			auto second_logger = IHTA::Instrumentation::LoggerRegistry::create_logger( logger_name, dev_mode );

			REQUIRE( logger == second_logger );
		}
	}

	SECTION( "New logger with sinks in registry" )
	{
		std::string logger_name = dev_mode ? "WithSinksDevMode" : "WithSinks";

		auto sink = std::make_shared<spdlog::sinks::null_sink_mt>( );

		IHTA::Instrumentation::LoggerRegistry::add_sink( sink );

		auto logger = IHTA::Instrumentation::LoggerRegistry::create_logger( logger_name, dev_mode );

		REQUIRE( !logger->sinks( ).empty( ) );
		auto sink_in_logger = std::find( logger->sinks( ).begin( ), logger->sinks( ).end( ), sink ) != logger->sinks( ).end( );
		REQUIRE( sink_in_logger );

		REQUIRE( logger->level( ) == ( dev_mode ? spdlog::level::level_enum::trace : spdlog::level::level_enum::info ) );

		SECTION( "No double creation" )
		{
			auto second_logger = IHTA::Instrumentation::LoggerRegistry::create_logger( logger_name, dev_mode );

			REQUIRE( logger == second_logger );
		}
	}
}

TEST_CASE( "IHTAInstrumentation::LoggerRegistry/SinkHandling", "[IHTAInstrumentation][SinkHandling][PrioHigh]" )
{
	IHTA::Instrumentation::LoggerRegistry::drop_all_registered_sinks( );

	auto sink = std::make_shared<spdlog::sinks::null_sink_mt>( );

	IHTA::Instrumentation::LoggerRegistry::add_sink( sink );

	auto logger1 = IHTA::Instrumentation::LoggerRegistry::create_logger( "SinkHandling1", false );

	IHTA::Instrumentation::LoggerRegistry::add_sink( sink );

	auto logger2 = IHTA::Instrumentation::LoggerRegistry::create_logger( "SinkHandling2", false );

	REQUIRE( logger1->sinks( ).size( ) == 1 );
	REQUIRE( logger2->sinks( ).size( ) == 1 );

	auto sink2 = std::make_shared<spdlog::sinks::null_sink_mt>( );

	IHTA::Instrumentation::LoggerRegistry::set_sinks_on_loggers( { sink, sink2 } );

	REQUIRE( logger1->sinks( ).size( ) == 2 );
	REQUIRE( logger2->sinks( ).size( ) == 2 );
}

TEST_CASE( "IHTAInstrumentation::LoggerRegistry/Logging", "[IHTAInstrumentation][Logging][PrioHigh]" )
{
	IHTA::Instrumentation::LoggerRegistry::drop_all_registered_sinks( );

	std::ostringstream oss;
	IHTA::Instrumentation::LoggerRegistry::add_sink( std::make_shared<spdlog::sinks::ostream_sink_mt>( oss ) );

	auto logger_name = GENERATE_RANDOM_STRING( 5 );
	auto logger_msg  = GENERATE_RANDOM_STRING( 5 );
	auto log_level   = GENERATE( table<std::string, spdlog::level::level_enum, bool>( { { "trace", spdlog::level::level_enum::trace, false },
	                                                                                    { "debug", spdlog::level::level_enum::debug, false },
	                                                                                    { "info", spdlog::level::level_enum::info, true },
	                                                                                    { "warning", spdlog::level::level_enum::warn, true },
	                                                                                    { "error", spdlog::level::level_enum::err, true },
	                                                                                    { "critical", spdlog::level::level_enum::critical, true } } ) );

	auto logger = IHTA::Instrumentation::LoggerRegistry::get_logger( logger_name );

	logger->log( std::get<1>( log_level ), logger_msg );

	std::smatch base_match;
	const auto log_result = oss.str( );
	const std::string regex_str( R"(^\[\d{4}-\d{2}-\d{2} \d+:\d+:\d+.\d+\] \[)" + escape_for_regex( logger_name ) + "\\] \\[" + std::get<0>( log_level ) + "\\] " +
	                             escape_for_regex( logger_msg ) );
	std::regex log_regex( regex_str );

	INFO( log_result );
	INFO( regex_str );

	REQUIRE( std::regex_search( log_result, log_regex ) == std::get<2>( log_level ) );
}

TEST_CASE( "IHTAInstrumentation::LoggerRegistry/RegexReplace", "[IHTAInstrumentation][RegexReplace][PrioLow]" )
{
	auto input = GENERATE( as<std::string> { }, "\\", "^", "$", "*", "+", "?", ".", "(", ")", "|", "{", "}", "[", "]" );
	REQUIRE_THAT( escape_for_regex( input ), Catch::Matchers::Equals( "\\" + input ) );
}

TEST_CASE( "IHTAInstrumentation::LoggerRegistry/SinkCreation", "[IHTAInstrumentation][SinkCreation][PrioHigh]" )
{
	IHTA::Instrumentation::LoggerRegistry::DefaultSinksConfig config;
	config.log_file_name      = GENERATE( as<std::string> { }, "", "file.log" );
	config.console_sink_level = GENERATE( spdlog::level::level_enum::trace, spdlog::level::level_enum::debug, spdlog::level::level_enum::info,
	                                      spdlog::level::level_enum::warn, spdlog::level::level_enum::err, spdlog::level::level_enum::critical );
	config.file_sink_level    = GENERATE( spdlog::level::level_enum::trace, spdlog::level::level_enum::debug, spdlog::level::level_enum::info,
	                                      spdlog::level::level_enum::warn, spdlog::level::level_enum::err, spdlog::level::level_enum::critical );

	// Format can only be tested implicitly by writing to the sink. This is not done yet.
	// config.console_sink_format =
	//     GENERATE( IHTA::Instrumentation::LogFormat::only_message, IHTA::Instrumentation::LogFormat::level_logger_message, IHTA::Instrumentation::LogFormat::va_like,
	//               IHTA::Instrumentation::LogFormat::spdlog_default, IHTA::Instrumentation::LogFormat::full_detail );
	// config.file_sink_format =
	//     GENERATE( IHTA::Instrumentation::LogFormat::only_message, IHTA::Instrumentation::LogFormat::level_logger_message, IHTA::Instrumentation::LogFormat::va_like,
	//               IHTA::Instrumentation::LogFormat::spdlog_default, IHTA::Instrumentation::LogFormat::full_detail );

	IHTA::Instrumentation::LoggerRegistry::drop_all_registered_sinks( );

	auto sinks = IHTA::Instrumentation::LoggerRegistry::create_default_sinks( config );

	if( GENERATE( true, false ) )
	{
		// call the function twice, this should not create more sinks.
		IHTA::Instrumentation::LoggerRegistry::create_default_sinks( config );
	}

	auto logger = IHTA::Instrumentation::LoggerRegistry::get_logger( "WithDefaultSinks" + config.log_file_name );

	REQUIRE_THAT( logger->sinks( ),
	              Catch::Matchers::AnyMatch( Catch::Matchers::Predicate<spdlog::sink_ptr>(
	                  []( spdlog::sink_ptr ptr ) -> bool { return static_cast<bool>( std::dynamic_pointer_cast<spdlog::sinks::stdout_color_sink_mt>( ptr ) ); },
	                  "Loggers sinks does not contain `stdout_color_sink_mt`" ) ) );

	REQUIRE( sinks.console_sink->level( ) == config.console_sink_level );

	if( config.log_file_name.empty( ) )
	{
		REQUIRE( logger->sinks( ).size( ) == 1 );
		REQUIRE( sinks.file_sink == nullptr );
	}
	else
	{
		REQUIRE( logger->sinks( ).size( ) == 2 );
		REQUIRE_THAT( logger->sinks( ),
		              Catch::Matchers::AnyMatch( Catch::Matchers::Predicate<spdlog::sink_ptr>(
		                  []( spdlog::sink_ptr ptr ) -> bool { return static_cast<bool>( std::dynamic_pointer_cast<spdlog::sinks::basic_file_sink_mt>( ptr ) ); },
		                  "Loggers sinks does not contain `stdout_color_sink_mt`" ) ) );
		REQUIRE( sinks.file_sink != nullptr );
		REQUIRE( sinks.file_sink->level( ) == config.file_sink_level );
	}
}

TEST_CASE( "IHTAInstrumentation::LoggerRegistry/Initiation", "[IHTAInstrumentation][Initiation][PrioHigh]" )
{
	auto log_level = GENERATE( as<std::string> { }, "trace", "debug", "info", "warning", "error", "critical" );

	IHTA::Instrumentation::LoggerRegistry::drop_all_registered_sinks( );

	auto spdlog_argument = std::string( "SPDLOG_LEVEL=InitLog=" ) + log_level;

	std::array<const char*, 2> argv( { "", spdlog_argument.c_str( ) } );
	IHTA::Instrumentation::LoggerRegistry::init_logging( 2, argv.data( ) );

	auto logger = IHTA::Instrumentation::LoggerRegistry::get_logger( "InitLog" );

	REQUIRE( logger->level( ) == spdlog::level::from_str( log_level ) );
}