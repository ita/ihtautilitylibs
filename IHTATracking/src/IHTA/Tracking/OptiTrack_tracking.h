/**
 * \cond DEV
 * \file OptiTrack_tracking.h
 * \brief Header for OptiTrack backend.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2021-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef INCLUDE_WATCHER_IHTA_OPTITRACK_TRACKING
#	define INCLUDE_WATCHER_IHTA_OPTITRACK_TRACKING

#	include <IHTA/Tracking/tracking.h>
#	include <atomic>
#	include <sstream>
#	include <thread>

// NatNet includes
#	include <NatNetClient.h>
#	include <NatNetTypes.h>

namespace IHTA::Tracking
{
	///
	/// \brief Concrete implementation for the Optitrack NatNet backend.
	///
	class COptiTrackTracking : public ITrackingInterface
	{
	public:
		///
		/// \brief Destructor for COptiTrackTracking.
		///
		virtual ~COptiTrackTracking( )
		{
			if( m_bConnected )
			{
				pNatNetClient->Uninitialize( );
				m_bConnected = false;
			}
		}

		///
		/// \copydoc ITrackingInterface::Initialize
		///
		bool Initialize( const std::string& sLocalAddress, const std::string& sServerAddress, int port ) override
		{
			pNatNetClient = std::make_unique<NatNetClient>( ConnectionType_Multicast );
			pNatNetClient->SetDataCallback(
			    []( sFrameOfMocapData* sFrame, void* pUserData )
			    {
				    const auto pClassPointer = static_cast<COptiTrackTracking*>( pUserData );

				    if( !pClassPointer )
					    return;

				    std::vector<STrackingDataPoint> dataPoints;
				    dataPoints.reserve( sFrame->nRigidBodies );

				    for( int i = 0; i < sFrame->nRigidBodies; i++ )
				    {
					    const auto& body = sFrame->RigidBodies[i];
					    dataPoints.emplace_back( );

					    dataPoints.back( ).bValid = true;

					    dataPoints.back( ).oLocation =
					        std::array<double, 3> { static_cast<double>( body.x ), static_cast<double>( body.y ), static_cast<double>( body.z ) };
					    dataPoints.back( ).oQuaternion = std::array<double, 4> { static_cast<double>( body.qx ), static_cast<double>( body.qy ),
						                                                         static_cast<double>( body.qz ), static_cast<double>( body.qw ) };
				    }

				    if( pClassPointer->oCallback )
				    {
					    pClassPointer->oCallback( dataPoints );
				    }
			    },
			    this );

			const int iError = pNatNetClient->Initialize( const_cast<char*>( sLocalAddress.c_str( ) ), const_cast<char*>( sServerAddress.c_str( ) ) );
			m_bConnected     = ( iError == ErrorCode_OK );

			return m_bConnected;
		}

		///
		/// \copydoc ITrackingInterface::RegisterCallback
		///
		bool RegisterCallback( std::function<void( const std::vector<STrackingDataPoint>& oDataPoints )> callback ) override
		{
			oCallback = callback;
			return false;
		}

		std::unique_ptr<NatNetClient> pNatNetClient; ///< Pointer to the NatNet client

		std::function<void( const std::vector<STrackingDataPoint>& )> oCallback; ///< Callback tread, calling the callback function when receiving a new data packet.

		bool bIsInitialized = false; ///< If true, the interface is initialized.

		bool m_bConnected = false; ///< If ture, the client is connected.
	};
} // namespace IHTA::Tracking
#endif

/// \endcond