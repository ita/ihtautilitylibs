/**
 * \cond DEV
 * \file profiler.cpp
 * \brief Main source file for the IHTA Profiler library.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2020-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <IHTA/Profiler/profiler.h>

// simulation scheduler includes
#include "profiler_data.h"
#include "profiler_key.h"
#include "profiler_thread_storage.h"

#include <IHTA/Profiler/profiler_types.h>

// std includes
#include <algorithm>
#include <fstream>
#include <mutex>
#include <ostream>
#include <sstream>
#include <stack>
#include <thread>
#include <unordered_map>

// misc includes
#include <nlohmann/json.hpp>

using namespace nlohmann;

namespace IHTA::Profiler
{
	template<class T>
	std::string ConvertToString( const std::vector<T>& data, int iPrecision = 8, const std::string& sSeperator = ", " )
	{
		std::stringstream ss;

		ss.precision( iPrecision );
		ss << std::scientific;

		ss << data.front( );

		std::for_each( data.begin( ) + 1, data.end( ), [&]( const T& val ) { ss << sSeperator << val; } );

		return ss.str( );
	}

	class ProfilerInstance;

#if _MSC_VER >= 1900 && FALSE
#	define MAGIC_STATICS
#	define THREAD_LOCAL thread_local
#else
#	define THREAD_LOCAL __declspec( thread )
#endif

	static THREAD_LOCAL SProfilerThreadStorage* THIS_THREAD = nullptr;

	///
	/// \brief Profiler class.
	///
	/// This singleton class keeps all profiler data.
	/// It should be used through the public interface defined in profiler.h.
	///
	/// It keeps a record of opened sections and saves the results of closed sections and event counts.
	///
	/// This implementation was inspired by [easy_profiler by yse](https://github.com/yse/easy_profiler).
	///
	class CProfiler
	{
	public:
		///
		/// \brief Get the  instance.
		/// \note This implementation is thread safe.
		///
		static CProfiler& GetInstance( );

		///
		/// \brief Add an event to the profiler.
		/// \param sName name of the section.
		/// \param sFunctionName name of the function that started the section.
		/// \param sFile name of the file in which the section was started.
		///
		void AddEventCount( const std::string& sName, const std::string& sFunctionName, const std::string& sFile )
		{
			if( !m_bEnabled )
				return;

			if( THIS_THREAD == nullptr )
				RegisterThread( );

			if( THIS_THREAD )
				THIS_THREAD->m_mEventCount[{ sName, sFunctionName, sFile, std::this_thread::get_id( ) }]++;
		}

		///
		/// \brief Start a section.
		///
		/// This function adds an open section to the stack for the current thread.
		/// \param oSection the section object to add to the profiler.
		/// \param sName name of the section.
		/// \param sFunctionName name of the function that started the section.
		/// \param sFile name of the file in which the section was started.
		///
		void StartSection( CSection& oSection, const std::string& sName, const std::string& sFunctionName, const std::string& sFile )
		{
			if( !m_bEnabled )
				return;

			if( THIS_THREAD == nullptr )
				RegisterThread( );

			oSection.Start( );

			if( THIS_THREAD )
				THIS_THREAD->m_msOpenSections[std::this_thread::get_id( )].push( { { sName, sFunctionName, sFile, std::this_thread::get_id( ) }, oSection } );
		}

		///
		/// \brief End the last opened section in the current thread.
		///
		void EndSection( )
		{
			if( !m_bEnabled )
				return;

			if( THIS_THREAD == nullptr )
				RegisterThread( );

			if( THIS_THREAD )
			{
				const auto iterator = THIS_THREAD->m_msOpenSections.find( std::this_thread::get_id( ) );
				if( iterator != THIS_THREAD->m_msOpenSections.end( ) )
				{
					auto& openSections = iterator->second;
					if( !openSections.top( ).second.get( ).Finished( ) )
						openSections.top( ).second.get( ).End( );

					THIS_THREAD->m_mSections[openSections.top( ).first].AddSection( openSections.top( ).second );

					openSections.pop( );
				}
			}
		}

		///
		/// \brief End all open sections for all threads.
		///
		void EndAllSections( )
		{
			std::lock_guard<std::mutex> lock( m_oMutex );

			for( auto& thread: m_mThreadStorage )
			{
				for( auto& key: thread.second.m_msOpenSections )
				{
					while( !key.second.empty( ) )
					{
						auto& openSections = key.second;

						if( !openSections.top( ).second.get( ).Finished( ) )
							openSections.top( ).second.get( ).End( );

						thread.second.m_mSections[openSections.top( ).first].AddSection( openSections.top( ).second );

						openSections.pop( );
					}
				}
			}
		}

		///
		/// \brief Name the current thread.
		///
		/// Only the first invocation of this has an effect.
		///
		void NameThread( const std::string& sName )
		{
			std::lock_guard<std::mutex> lock( m_oMutex );
			if( m_mThreadNames.find( std::this_thread::get_id( ) ) == m_mThreadNames.end( ) )
				m_mThreadNames[std::this_thread::get_id( )] = sName;
		}

		///
		/// \brief Add a value to the profiler.
		///
		/// The value must be of type: bool, char, int, float, double, string.
		/// \param sName name of the value.
		/// \param value the value to be saved in the profiler.
		/// \param sFunctionName name of the function that issued the event.
		/// \param sFile name of the file in which the event was issued.
		///
		void AddValue( const std::string& sName, const ValueData& value, const std::string& sFunctionName, const std::string& sFile )
		{
			if( !m_bEnabled )
				return;

			if( THIS_THREAD == nullptr )
				RegisterThread( );

			if( THIS_THREAD )
				THIS_THREAD->m_mValues[{ sName, sFunctionName, sFile, std::this_thread::get_id( ) }].AddValue( value );
		}

		///
		/// \brief Register a thread with a thread storage.
		///
		void RegisterThread( ) { THIS_THREAD = &GetThreadStorageReferenceByID( std::this_thread::get_id( ) ); }

		///
		/// \brief Thread save access to thread storage map.
		/// \param oThreadId ID of the thread storage to be retrieved.
		/// \return reference to the desired thread storage.
		///
		SProfilerThreadStorage& GetThreadStorageReferenceByID( std::thread::id oThreadId )
		{
			std::lock_guard<std::mutex> lock( m_oMutex );

			return m_mThreadStorage[oThreadId];
		}

		///
		/// \brief Parses the profiler data as a JSON file.
		/// \return a string stream containing the profiler data as a JSON file.
		///
		std::stringstream ProfilerToJSON( )
		{
			json jnRoot;

			jnRoot["CPU Frequency"] = static_cast<double>( std::chrono::high_resolution_clock::period::den / std::chrono::high_resolution_clock::period::num );

			json jnSection;
			json jnEventCount;
			json jnValue;

			// Disable profiling while we write to file.
			m_bEnabled = false;

			// Wait a bit for sections to end
			const auto start = std::chrono::high_resolution_clock::now( );
			while( std::chrono::duration<double>( std::chrono::high_resolution_clock::now( ) - start ).count( ) < 0.05 )
			{
			}

			// Now we force all sections to end.
			EndAllSections( );

			// Lock the mutex as we want thread safe access to the thread storage.
			std::lock_guard<std::mutex> lock( m_oMutex );

			for( const auto& threadStorage: m_mThreadStorage )
			{
				for( const auto& sectionPair: threadStorage.second.m_mSections )
				{
					jnSection.push_back( ProfileSectionToJSON( sectionPair, m_mThreadNames ) );
				}

				for( const auto& eventCountPair: threadStorage.second.m_mEventCount )
				{
					jnEventCount.push_back( ProfileEventCountToJSON( eventCountPair, m_mThreadNames ) );
				}

				for( const auto& valuePair: threadStorage.second.m_mValues )
				{
					jnValue.push_back( ProfileValuesToJSON( valuePair, m_mThreadNames ) );
				}
			}

			jnRoot["Profile Sections"]    = jnSection;
			jnRoot["Profile EventCounts"] = jnEventCount;
			jnRoot["Profile Values"]      = jnValue;

			// Reenable profiling
			m_bEnabled = true;

			std::stringstream ss;
			ss << jnRoot;
			return ss;
		}

		///
		/// \brief Parses the given eventCount pair to a JSON Node.
		/// \param eventCountPair pair with the Key and the ProfileEventCount to be parsed.
		/// \param mThreadNames map, containing the names of the threads.
		/// \return a JSON Node with the data of the given eventCount pair.
		///
		json ProfileEventCountToJSON( const std::pair<ProfilerKey, ProfileEventCount>& eventCountPair,
		                              const std::unordered_map<std::thread::id, std::string>& mThreadNames )
		{
			std::string sThreadName;

			const auto threadNameIterator = mThreadNames.find( eventCountPair.first.iThreadId );

			if( threadNameIterator != mThreadNames.end( ) )
				sThreadName = threadNameIterator->second;

			json jnRoot;
			jnRoot["Name"]          = eventCountPair.first.sName;
			jnRoot["File Name"]     = eventCountPair.first.sFile;
			jnRoot["Function Name"] = eventCountPair.first.sFunction;
			jnRoot["Thread Id"]     = std::hash<std::thread::id>( )( eventCountPair.first.iThreadId ) % std::numeric_limits<int32_t>::max( );

			if( !sThreadName.empty( ) )
				jnRoot["Thread Name"] = sThreadName;

			jnRoot["Count"] = eventCountPair.second.vTimeStamps.size( );

			std::vector<double> vdTimeStamps( eventCountPair.second.vTimeStamps.size( ) );

			std::transform( eventCountPair.second.vTimeStamps.begin( ), eventCountPair.second.vTimeStamps.end( ), vdTimeStamps.begin( ),
			                [&]( const std::chrono::high_resolution_clock::time_point& time ) -> double
			                { return std::chrono::duration<double>( time - m_dStartTime ).count( ); } );

			constexpr auto dCPUFrequency = static_cast<double>( std::chrono::high_resolution_clock::period::den ) / std::chrono::high_resolution_clock::period::num;
			const int precision          = 1 + std::ceil( std::log10( dCPUFrequency ) );

			jnRoot["Time Stamps"] = ConvertToString( vdTimeStamps, precision, "," );

			return jnRoot;
		}

		///
		/// \brief Parses the given section pair to a JSON Node.
		///
		/// This function also calculates and add the min, max, mean, variance and standard deviation of the section to the JSON Node.
		/// \param sectionPair pair with the Key and the ProfileSection to be parsed.
		/// \param mThreadNames map, containing the names of the threads.
		/// \return a JSON Node with the data of the given section pair.
		///
		json ProfileSectionToJSON( const std::pair<ProfilerKey, ProfileSection>& sectionPair, const std::unordered_map<std::thread::id, std::string>& mThreadNames )
		{
			std::string sThreadName;

			const auto threadNameIterator = mThreadNames.find( sectionPair.first.iThreadId );

			if( threadNameIterator != mThreadNames.end( ) )
				sThreadName = threadNameIterator->second;

			json jnRoot;
			jnRoot["Name"]          = sectionPair.first.sName;
			jnRoot["File Name"]     = sectionPair.first.sFile;
			jnRoot["Function Name"] = sectionPair.first.sFunction;
			jnRoot["Thread Id"]     = std::hash<std::thread::id>( )( sectionPair.first.iThreadId ) % std::numeric_limits<int32_t>::max( );

			if( !sThreadName.empty( ) )
				jnRoot["Thread Name"] = sThreadName;

			jnRoot["Count"] = sectionPair.second.vpStartEndTimes.size( );

			std::vector<double> vdStartTime( sectionPair.second.vpStartEndTimes.size( ) );
			std::vector<double> vdEndTime( sectionPair.second.vpStartEndTimes.size( ) );

			std::transform( sectionPair.second.vpStartEndTimes.begin( ), sectionPair.second.vpStartEndTimes.end( ), vdStartTime.begin( ),
			                [&]( const std::pair<std::chrono::high_resolution_clock::time_point, std::chrono::high_resolution_clock::time_point>& interval ) -> double
			                { return std::chrono::duration<double>( interval.first - m_dStartTime ).count( ); } );

			std::transform( sectionPair.second.vpStartEndTimes.begin( ), sectionPair.second.vpStartEndTimes.end( ), vdEndTime.begin( ),
			                [&]( const std::pair<std::chrono::high_resolution_clock::time_point, std::chrono::high_resolution_clock::time_point>& interval ) -> double
			                { return std::chrono::duration<double>( interval.second - m_dStartTime ).count( ); } );

			constexpr auto dCPUFrequency = static_cast<double>( std::chrono::high_resolution_clock::period::den ) / std::chrono::high_resolution_clock::period::num;
			const int precision          = 1 + std::ceil( std::log10( dCPUFrequency ) );

			jnRoot["Start Times"] = ConvertToString( vdStartTime, precision, "," );
			jnRoot["End Times"]   = ConvertToString( vdEndTime, precision, "," );

			return jnRoot;
		}

		///
		/// \brief Parses the given ProfileValue pair to a JSON Node.
		/// \param valuePair pair with the Key and the ProfileEventCount to be parsed.
		/// \param mThreadNames map, containing the names of the threads.
		/// \return a JSON Node with the data of the given ProfileValue pair.
		///
		json ProfileValuesToJSON( const std::pair<ProfilerKey, ProfileValue>& valuePair, const std::unordered_map<std::thread::id, std::string>& mThreadNames )
		{
			std::string sThreadName;

			const auto threadNameIterator = mThreadNames.find( valuePair.first.iThreadId );

			if( threadNameIterator != mThreadNames.end( ) )
				sThreadName = threadNameIterator->second;

			json jnRoot;
			jnRoot["Name"]          = valuePair.first.sName;
			jnRoot["File Name"]     = valuePair.first.sFile;
			jnRoot["Function Name"] = valuePair.first.sFunction;
			jnRoot["Thread Id"]     = std::hash<std::thread::id>( )( valuePair.first.iThreadId ) % std::numeric_limits<int32_t>::max( );

			if( !sThreadName.empty( ) )
				jnRoot["Thread Name"] = sThreadName;

			jnRoot["Count"] = valuePair.second.Size( );

			json valueNodes;

			// store the tim stamps
			switch( valuePair.second.GetDataType( ) )
			{
				case DataType::Bool:
					valueNodes = ValueDataToJSON<bool>( valuePair.second );
					break;
				case DataType::Char:
					valueNodes = ValueDataToJSON<char>( valuePair.second );
					break;
				case DataType::Int:
					valueNodes = ValueDataToJSON<int>( valuePair.second );
					break;
				case DataType::UnsignedInt:
					valueNodes = ValueDataToJSON<unsigned int>( valuePair.second );
					break;
				case DataType::UnsignedLongInt:
					valueNodes = ValueDataToJSON<unsigned long int>( valuePair.second );
					break;
				case DataType::Float:
					valueNodes = ValueDataToJSON<float>( valuePair.second );
					break;
				case DataType::Double:
					valueNodes = ValueDataToJSON<double>( valuePair.second );
					break;
				default:
					throw std::runtime_error( "Unknown data type" );
			}

			jnRoot.insert( valueNodes.begin( ), valueNodes.end( ) );

			return jnRoot;
		}

		///
		/// \brief Parse a ProfileValue to two JSONNodes for data and time stamp.
		/// \tparam T type of the desired ProfileValue.
		/// \param oProfileValue the ProfileValue to be parsed.
		/// \return returns a tuple with the JSONNode for the time stamp and data.
		///
		template<class T>
		json ValueDataToJSON( const ProfileValue& oProfileValue )
		{
			std::vector<std::pair<T, double>> dataVector = oProfileValue.GetData<T>( m_dStartTime );

			std::vector<T> vTData( dataVector.size( ) );
			std::vector<double> vdTimeStamps( dataVector.size( ) );

			std::transform( dataVector.begin( ), dataVector.end( ), vTData.begin( ), []( const std::pair<T, double>& p ) -> T { return p.first; } );

			std::transform( dataVector.begin( ), dataVector.end( ), vdTimeStamps.begin( ), []( const std::pair<T, double>& p ) -> double { return p.second; } );

			constexpr auto dCPUFrequency = static_cast<double>( std::chrono::high_resolution_clock::period::den ) / std::chrono::high_resolution_clock::period::num;
			const int precision          = 1 + std::ceil( std::log10( dCPUFrequency ) );

			json jnRoot;
			jnRoot["Time Stamps"] = ConvertToString( vdTimeStamps, precision, "," );
			jnRoot["Data"]        = ConvertToString( vTData, 8, "," );
			return jnRoot;
		}

	private:
#ifndef MAGIC_STATICS
		friend ProfilerInstance;
#endif

		CProfiler( ) { m_dStartTime = std::chrono::high_resolution_clock::now( ); }

		~CProfiler( ) = default;

		CProfiler( const CProfiler& other ) = delete;

		CProfiler& operator=( const CProfiler& other ) = delete;

		std::chrono::high_resolution_clock::time_point m_dStartTime;

		///
		/// \brief Map, storing the thread storage.
		///
		std::unordered_map<std::thread::id, SProfilerThreadStorage> m_mThreadStorage;

		///
		/// \brief Map, storing the names of named threads.
		///
		std::unordered_map<std::thread::id, std::string> m_mThreadNames;

		///
		/// \brief Mutex for write access.
		///
		std::mutex m_oMutex;

		///
		/// \brief True, if profiling is activated.
		///
		/// This is used to stop profiling while we save data.
		///
		bool m_bEnabled = true;
	};

	class ProfilerInstance final
	{
		friend CProfiler;
		CProfiler instance;
	} PROFILE_MANAGER;

	CProfiler& CProfiler::GetInstance( )
	{
#ifdef MAGIC_STATICS
		static CProfiler instance;
		return instance;
#else
		return PROFILE_MANAGER.instance;
#endif
	}

	void CSection::Start( )
	{
		m_dBegin = std::chrono::high_resolution_clock::now( );
	}

	void CSection::End( )
	{
		m_dEnd = std::chrono::high_resolution_clock::now( );
	}

	bool CSection::Finished( ) const
	{
		return m_dEnd >= m_dBegin;
	}

	CSection::~CSection( )
	{
		if( !Finished( ) )
			IHTA::Profiler::EndSection( );
	}

	void StartSection( CSection& oSection, const std::string& sName, const std::string& sFunctionName, const std::string& sFile )
	{
		CProfiler::GetInstance( ).StartSection( oSection, sName, sFunctionName, sFile );
	}

	void EndSection( )
	{
		CProfiler::GetInstance( ).EndSection( );
	}

	void EndAllSections( )
	{
		CProfiler::GetInstance( ).EndAllSections( );
	}

	void AddEventCount( const std::string& sName, const std::string& sFunctionName, const std::string& sFile )
	{
		CProfiler::GetInstance( ).AddEventCount( sName, sFunctionName, sFile );
	}

	void StoreValue( const std::string& sName, const ValueData& valueData, const std::string& sFunctionName, const std::string& sFile )
	{
		CProfiler::GetInstance( ).AddValue( sName, valueData, sFunctionName, sFile );
	}

	void NameThread( const std::string& sName )
	{
		CProfiler::GetInstance( ).NameThread( sName );
	}

	std::stringstream ProfilerToJson( )
	{
		return CProfiler::GetInstance( ).ProfilerToJSON( );
	}

} // namespace IHTA::Profiler

/// \endcond
