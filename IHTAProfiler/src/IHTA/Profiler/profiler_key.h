/**
 * \cond DEV
 * \file profiler_key.h
 * \brief Header containing the key data structure for the internal profiler data map.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2020-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef INCLUDE_WATCHER_IHTA_PROFILER_KEY
#	define INCLUDE_WATCHER_IHTA_PROFILER_KEY

// std includes
#	include <thread>

namespace IHTA::Profiler
{
	///
	/// \brief Key for the Profiler maps.
	///
	struct ProfilerKey
	{
		std::string sName;
		std::string sFunction;
		std::string sFile;
		std::thread::id iThreadId;

		bool operator==( const ProfilerKey& other ) const
		{
			return ( sName == other.sName && sFunction == other.sFunction && sFile == other.sFile && iThreadId == other.iThreadId );
		}
	};
} // namespace IHTA::Profiler

namespace std
{
	template<>
	struct hash<IHTA::Profiler::ProfilerKey>
	{
		std::size_t operator( )( const IHTA::Profiler::ProfilerKey& k ) const
		{
			using std::hash;
			using std::size_t;
			using std::string;

			return ( ( ( hash<string>( )( k.sName ) ^ ( hash<string>( )( k.sFunction ) << 1 ) ) >> 1 ) ^ ( hash<string>( )( k.sFile ) << 1 ) >> 1 ) ^
			       ( hash<std::thread::id>( )( k.iThreadId ) << 1 );
		}
	};
} // namespace std

#endif // INCLUDE_WATCHER_IHTA_PROFILER_KEY

/// \endcond