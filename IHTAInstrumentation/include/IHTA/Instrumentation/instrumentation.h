/**
 * \file instrumentation.h
 * \brief Main header file for the IHTA Instrumentation library.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2023-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef INCLUDE_WATCHER_IHTA_INSTRUMENTATION
#define INCLUDE_WATCHER_IHTA_INSTRUMENTATION

#include "log_format.h"

#include <IHTA_instrumentation_export.h>
#include <memory>
#include <spdlog/spdlog.h>
#include <string>
#include <vector>

// clang-format off
#include <spdlog/fmt/bundled/ostream.h>
// clang-format on

///
/// \brief Namespace containing all classes and methods belonging to the IHTA Instrumentation library.
///
namespace IHTA::Instrumentation
{
	///
	/// \brief Registry to hold all loggers and sinks of the library.
	///
	/// spdlog cannot share loggers over library/executable boundaries.
	/// By using this registry the loggers of all modules are contained in one library and can be adapted all together.
	/// \see https://github.com/gabime/spdlog/wiki/How-to-use-spdlog-in-DLLs
	///
	class IHTAINSTRUMENTATION_EXPORT LoggerRegistry
	{
	public:
		///
		/// \brief Instantiate the logging.
		///
		/// This should be called once at the beginning of an executable which uses the logging.
		/// Environment settings will be overwritten by command line arguments if available.
		/// Command line arguments will be overwritten by programmatic settings, for example \c t_developer_mode of #create_logger will set all loggers to trace.
		/// \see https://github.com/gabime/spdlog#load-log-levels-from-the-env-variable-or-argv
		///
		static void init_logging( );

		///
		/// \copydoc init_logging()
		/// \param argc command line argument count.
		/// \param argv command line argument vector.
		///
		static void init_logging( int argc, char **argv );

		///
		/// \copydoc init_logging(int, char**)
		///
		static void init_logging( int argc, const char **argv );

		///
		/// \brief Configuration for creating default sinks using #create_default_sinks.
		///
		struct DefaultSinksConfig
		{
			std::string log_file_name                    = "";
			std::string file_sink_format                 = LogFormat::full_detail;
			spdlog::level::level_enum file_sink_level    = spdlog::level::level_enum::trace;
			std::string console_sink_format              = LogFormat::level_logger_message;
			spdlog::level::level_enum console_sink_level = spdlog::level::level_enum::info;
		};

		///
		/// \brief Container for sinks created by #create_default_sinks.
		///
		struct SinkContainer
		{
			spdlog::sink_ptr console_sink;
			spdlog::sink_ptr file_sink;
		};

		///
		/// \brief Create default sinks in the registry.
		///
		/// This means a spdlog::sinks::stdout_color_sink_mt and if requested a spdlog::sinks::basic_file_sink_mt.
		/// If file name for the log file is empty, no file logger will be created.
		/// \param t_config config for the sinks.
		/// \returns container with created sink pointers.
		///
		static SinkContainer create_default_sinks( const DefaultSinksConfig &t_config = { } );

		///
		/// \brief Create a new logger with the given name.
		///
		/// Sinks in #sink_registry will be added to the created logger.
		/// \param t_logger_name the name of the new logger.
		/// \param t_developer_mode specifies if the logger should be in developer mode i.e. trace.
		/// \return std::shared_ptr<spdlog::logger> created shared pointer logger instance.
		///
		static std::shared_ptr<spdlog::logger> create_logger( std::string t_logger_name, bool t_developer_mode = false );

		///
		/// \brief Get a logger via its name.
		///
		/// If the logger with the requested name does not exist, this method creates a new logger via #create_logger.
		/// \param t_logger_name the name of the logger to get.
		/// \param t_developer_mode specifies if the logger should be in developer mode i.e. trace.
		/// \return std::shared_ptr<spdlog::logger> requested shared pointer logger instance.
		///
		static std::shared_ptr<spdlog::logger> get_logger( std::string t_logger_name, bool t_developer_mode = false );

		///
		/// \brief Add a sink to the sink registry.
		///
		/// \param t_sink sink to add to the registry.
		///
		static void add_sink( spdlog::sink_ptr t_sink );

		///
		/// \brief Set the given sinks on all loggers.
		///
		/// \param t_sinks sinks to add to all loggers.
		///
		static void set_sinks_on_loggers( std::vector<spdlog::sink_ptr> t_sinks );

		///
		/// \brief Drop all sinks that are registered.
		///
		static void drop_all_registered_sinks( );

	private:
		///
		/// \brief Registry of all sinks known by the registry.
		///
		/// All sinks known by the registry will be added to loggers created with #create_logger.
		///
		static std::vector<spdlog::sink_ptr> sink_registry;
	};
} // namespace IHTA::Instrumentation

#endif