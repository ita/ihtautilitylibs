#include "lib.h"

void lib::func( )
{
	EASY_FUNCTION( );

	LIB_ERROR( "error" );
	localSleep( );
	LIB_TRACE( "trace" );
}

void lib::localSleep( uint64_t magic )
{
	EASY_FUNCTION( );
	volatile int i = 0;
	for( ; i < magic; ++i )
		;
}