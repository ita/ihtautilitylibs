/**
 * \cond DEV
 * \file profiler_thread_storage.h
 * \brief Header containing the data structure holding the data maps for each thread.
 * \author Pascal Palenda ppa@akustik.rwth-aachen.de
 * \copyright
 * Copyright 2020-2024 Institute for Hearing Technology and Acoustics (IHTA) RWTH Aachen University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef INCLUDE_WATCHER_IHTA_PROFILER_THREAD_STORAGE
#	define INCLUDE_WATCHER_IHTA_PROFILER_THREAD_STORAGE

// std includes
#	include <stack>
#	include <thread>
#	include <unordered_map>

// simulation scheduler includes
#	include "profiler_data.h"
#	include "profiler_key.h"

namespace IHTA::Profiler
{
	struct SProfilerThreadStorage
	{
		///
		/// \brief Record of all ProfileSection%s.
		///
		std::unordered_map<ProfilerKey, ProfileSection> m_mSections;

		///
		/// \brief Record of all ProfileEventCount%s
		///
		std::unordered_map<ProfilerKey, ProfileEventCount> m_mEventCount;

		///
		/// \brief Record of all ProfileValues%s
		///
		std::unordered_map<ProfilerKey, ProfileValue> m_mValues;

		///
		/// \brief Record of all currently open CSection%s keyed by their thread.
		///
		std::unordered_map<std::thread::id, std::stack<std::pair<ProfilerKey, std::reference_wrapper<CSection>>>> m_msOpenSections;
	};
} // namespace IHTA::Profiler

#endif // INCLUDE_WATCHER_IHTA_PROFILER_THREAD_STORAGE

/// \endcond