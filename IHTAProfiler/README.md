# IHTA Profiler

The IHTA Profiler is a tread-safe profiling library for in-app profiling.
The macros defined in `include/IHTA/Profiler/profiler.h` can be inserted into source code and the corresponding events will be stored.
The stored profile can then be converted into a proprietary json format and written to file.
Using the included matlab `ProfileReader`, the proprietary json file can be loaded.
The `ProfileReader` allows querying of the stored data as well as converting the profile into the [*Chrome Trace Event*](https://docs.google.com/document/d/1CvAClvFfyA5R-PhYUmn5OOQtYMH4h6I0nSsKchNAySU/preview#heading=h.f2hfbjkv9dc9) format.
These traces can than be opened either in the [Chrome trace viewer](chrome://tracing/) (only available with the chrome browser) or using the [Perfetto](https://ui.perfetto.dev/) viewer.

The inclusion of this profiler in your code will have a performance effect.
The exact amount has not been benchmarked yet.

## Usage

In the following, the main functions od the profiler will be explained shortly:

### `IHTA_PROFILER_EVENT_COUNT( NAME )`

Saves an instantaneous event who's number of occurrences are counted.
This macro accepts the `NAME` of the event as argument.

### `IHTA_PROFILER_VALUE( NAME, VALUE )`

Saves an instantaneous event with a related value.
This value can be of type Bool, Char, Int, UnsignedInt, UnsignedLongInt, Float or Double.
This macro accepts the `NAME` of the event and the variable that should be stored as argument.

### `IHTA_PROFILER_SECTION( NAME )` & `IHTA_PROFILER_FUNCTION( )`

Saves the time, a section of code needs to be executed.
Section events are scoped, as a result, they last until the end of the scope they have been initialized.
However, they can be ended before using `IHTA_PROFILER_END_SECTION( )`.
This macro accepts the `NAME` of the event as argument.

`IHTA_PROFILER_FUNCTION( )` is a convenience macro that starts the a section called "Full", to profile the full runtime of a function.

### `IHTA_PROFILER_END_SECTION( )`

Ends the last opened section.

### `IHTA_PROFILER_NAME_THREAD( NAME )` & `IHTA_PROFILER_NAME_THREAD_MAIN( )`

Names the currently executing thread `NAME`.
This can be useful when analyzing the profile later, to identify the threads.
This macro accepts the `NAME` of the event as argument.

`IHTA_PROFILER_NAME_THREAD_MAIN( )` is a convenience macro that names the current thread "Main".

### `std::stringstream IHTA::Profiler::ProfilerToJson( )`

Returns a stringstream with the profiler data encoded in json.
